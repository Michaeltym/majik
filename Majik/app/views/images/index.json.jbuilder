json.array!(@images) do |image|
  json.extract! image, :id, :image_name, :parent_id, :user_id, :version, :image_path, :filter_id
  json.url image_url(image, format: :json)
end
